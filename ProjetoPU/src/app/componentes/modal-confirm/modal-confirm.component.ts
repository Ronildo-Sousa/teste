import { Component, OnInit } from '@angular/core';
import { ModalController, NavController } from '@ionic/angular';
import { ApiService } from 'src/app/services/api.service';

@Component({
  selector: 'app-modal-confirm',
  templateUrl: './modal-confirm.component.html',
  styleUrls: ['./modal-confirm.component.scss'],
})
export class ModalConfirmComponent implements OnInit {
  id = '';
  nome: string;
  cpf: string;

  constructor(private modalCtrl: ModalController, private apiService: ApiService, private navCtrl: NavController,) {
    this.id = JSON.parse(localStorage.getItem('entrega_id'));
  }

  setName($event) {
    this.nome = $event.target.value
  }
  setCpf($event) {
    this.cpf = $event.target.value
  }

  confirm() {
    this.apiService.ConfirmaEntrega(this.id, {name_receiver: this.nome, cpf_receiver: this.cpf})
      .then(res => {
        this.dismiss();
        this.gotoMainEntregas();
      })
  }

  gotoMainEntregas() {
    this.navCtrl.navigateRoot('main-entregador/tabs/entregas');
  }

  ngOnInit() {}

  dismiss() {

    this.modalCtrl.dismiss({
      'dismissed': true
    });
  }
}
